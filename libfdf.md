---
project: libfdf
summary: Library to support the Flexible Data Format (FDF)
author:  Alberto Garcia, Nick Papior, Raul de la Cruz
project_dir: src
page_dir: doc/pages
output_dir: /tmp/fdf-docs
exclude: 
preprocessor: gfortran -E -P
preprocess: true
project_website: http://gitlab.com/siesta-project/libraries/libfdf
email: albertog@icmab.es
extensions: f90
            F90
docmark: +	    
docmark_alt: *
predocmark: >
predocmark_alt: <
source: false
graph: false
search: true
license: bsd
display: public
	 protected
	 private
extra_filetypes: sh #
md_extensions: markdown.extensions.toc
---

The library provides an API for flexible input  handling for Fortran 
programs.

For more information, check out the [Overview](./page/index.html).
