title: Overview

FDF stands for Flexible Data Format, designed within the Siesta
project to simplify the handling of input options. It is based on
a keyword/value paradigm (including physical units when relevant), supplemented by
blocks for arbitrarily complex blobs of data and lists.

LibFDF is the official implementation of the FDF specifications for use in client codes.
At present the FDF format is used extensively by Siesta, and it has
been an inspiration for several other code-specific input formats.

New input options can be implemented very easily. When a keyword is not present
in the FDF file the corresponding program variable is assigned a
pre-programmed default value. This enables programmers of client codes
to insert new input statements anywhere in the code, without taking
care of ``reserving a slot'' in a possibly already crowded
fixed-format input file.


