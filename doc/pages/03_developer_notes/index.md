title: Developer Notes

(Documentation work in progress)

## Creating the database

The main routine is [[fdf_read(proc)]], which opens the main file and starts
adding material to the fdf object.  If an `%include` line is found,
the routine calls itself again with the new file.

Support for the `Label ... Label < other_file` idiom complicates the
code quite a bit, since an alternative hierarchy of possibly nested
file reads must be instantiated. This is done with the [[fdf_readlabel(proc)]]
routine, which is also recursive and may call `fdf_read` again.

## Parsing

Low-level support for all the parsing needed is provided by the
[[fdf_parse(module)]] module, which is useful in itself and could potentially be
made into an independent library. The basic idiom is to `digest` an
input line into tokens, tagging them as integers, reals, names
(strings), etc. There are query functions for number of tokens of each
kind, plus extractors for them. Also, a quite powerful [[match(proc)]]
routine can be used to identify at once the structure of a line.

### Things to do

* Make a cleaner separation of the reading and querying stages
* Abstract some more the basic operations related to the fdf database object
* Use better names for some of the procedures
* (Maybe) Allow multiple instances of fdf-objects in a single program
