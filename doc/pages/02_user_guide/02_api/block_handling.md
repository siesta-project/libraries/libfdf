title: Block Handling

### Scanning block contents

Block content can be flexibly handled by means of a pointer
to a derived type 'block_fdf'. Typical usage:

```fortran
     use fdf
     type(block_fdf)            :: bfdf
     type(parsed_line), pointer :: pline

     if (fdf_block('SomeBlock', bfdf)) then
       do while(fdf_bline(bfdf, pline))
         (process line 'integers|reals|values|names ...')
       enddo
       call fdf_bclose(bfdf)
     endif
```

The subroutine 'fdf_block' returns in 'bfdf' a structure used to read
the contents of the block.

Routine fdf_bline returns in 'pline' a parsed version of the next
non-blank, non-comment line from the block, unless there are no more
lines, in which case it returns .FALSE. and 'pline' is undefined.

Routine fdf_bclose runs the remaining lines in the block and ensures
the log may be used as input in subsequent entries.

### Processing block contents

Once a block line has been parsed, a number of query and data extraction
routines can be used:

* [[fdf_bnnames(proc)]] : Returns the number of strings ("names") in the line.
* [[fdf_bnintegers(proc)]]
* [[fdf_bnreals(proc)]]
* [[fdf_bnvalues(proc)]]
* [[fdf_bntokens(proc)]]

* [[fdf_bnames(proc)]]: Extracts a name from its correlative index.
* ...

Routine [[fdf_bbackspace(proc)]]  moves the internal pointer of 'block_fdf'
structure to the previous line returned.

Routine [[fdf_brewind(proc)]]  moves the internal pointer of 'block_fdf' structure
to the beginning of the block.
