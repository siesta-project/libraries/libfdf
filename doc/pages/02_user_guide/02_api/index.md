title: libFDF API

[TOC]

Access to the library's functionality is provided by importing the [[fdf(module)]] module:

```
use fdf
```

which exports the interfaces to the public user-level routines and other symbols.

## Exported types

* A block handle [[block_fdf(type)]]
* A parsed-line handle [[parsed_line(type)]]

## Initialization routine

It is necessary to call the [[fdf_init(proc)]] routine before any other operations.
When working in parallel, only the master node has to call the routine (see [this file](../README_MPI.html) for
more information.

## Main routines

These perform queries and extract data from the FDF file, in accordance with the [FDF specifications](./fdf.Standard.html)

### Label queries

* The routine [[fdf_defined(proc)]] returns true if a given label is present in the fdf file.

### Scalar values

* The interface [[fdf_get(proc)]] is overloaded to cover all relevant data types, and optionally handles units.

### Block handling

* Routine [[fdf_block(proc)]] returns true if a given block is present
  in the fdf file, and also instantiates a block handle for further
  processing. A full example of block processing can be found [here](./block_handling.html).
  
### Lists

A full example of list processing can be found [here](./list_handling.html).




