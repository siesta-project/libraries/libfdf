title: Installation

## Installation of libFDF with CMake


```
    cmake -S. -B_build -DCMAKE_INSTALL_PREFIX=/path/to/install/directory
    cmake --build _build
    cmake --build _build --target test  # To run a simple test
    cmake --install _build
```

## Installation of libFDF with autotools

```
    ./configure --prefix=/path/to/install/directory
    make
    make check
    make install
```

## Compiling user programs with CMake

Just use the standard CMake idiom in your CMakeLists.txt file:

```
    add_executable(your_program your_sources)
    find_package(libfdf REQUIRED)
    target_link_libraries(your_program libfdf::libfdf)
```

The above assumes that the installation directory for libfdf can be
found by CMake. This can be achieved by adding it to the
CMAKE_PREFIX_PATH CMake or enviroment variable:

```
    cmake -S. -B_your_build -DCMAKE_PREFIX_PATH="$FDF_ROOT" .......
    CMAKE_PREFIX_PATH=$FDF_ROOT cmake -S. -B_your_build .......
```

## Compiling user programs using pkg-config

Both the CMake and the autotools building systems install a libfdf.pc
file in the installation hierarchy. This file can be used by
pkg-config to determine the libfdf module path and the appropriate
link incantation. For autotools the modules will be in
`$FDF_ROOT/include` and the library files in `$FDF_ROOT/lib`, but with
CMake this might vary. In all cases, assuming that PKG_CONFIG_PATH is
set correctly,

```
   pkg-config --cflags libfdf
   pkg-config --libs libfdf
```

will produce the appropriate include and link strings.










