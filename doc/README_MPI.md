title: MPI operation

For MPI operation, the "reading node" has to call the fdf initialization routine,
serialize the fdf data structure, and broadcast it. Other nodes receive the
structure and de-serialize it. In the following snippet we assume that 0 is the
reading node:

```fortran
      if (Node .eq. 0) then
         call fdf_init(filein, fileout)
      else
         !                                                                                                                                   
      endif
#ifdef MPI      
      call broadcast_fdf_struct(0,mpi_comm_world)
      !

      ! Required in MPI operation. Choose one of the two, according
      ! to whether the default unit handler is fine or not
        !! call fdf_set_unit_handler( custom_unit_handler )
        !! call fdf_set_unit_handler( fdf_legacy_unit_handler )
#endif
```

The routine `broadcast_fdf_struct` can be found in file `broadcast_fdf_struct.F90`
in the `doc` directory of the fdf distribution. It should be general enough for most codes.

