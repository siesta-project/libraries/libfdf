!
!     Shows FDF units capabilities
!
PROGRAM test_ambiguous_units
  USE fdf
  use units_ambiguous_m, only: inquire_unit
  USE fdf_prec
  implicit none
  
  real(dp)                   :: mass, bfield, ion_energy

! Initialize
  call fdf_init('ambiguous-units.fdf', 'ambiguous-units.out')

  call fdf_set_unit_handler(inquire_unit)

  mass = fdf_physical('body-mass', 0.5d0, 'Kg')
  write(6,*) 'Body-mass: ', mass

  bfield = fdf_physical('Bfield', 4.5d0, 'bfield:G')
  write(6,*) 'Bfield:', bfield

  ion_energy = fdf_physical('ion-energy', 40.d0, 'J')
  write(6,*) 'ion_energy:', ion_energy

  call fdf_shutdown()

END PROGRAM TEST_AMBIGUOUS_UNITS
