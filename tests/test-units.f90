!
!     Shows FDF units capabilities
!
PROGRAM test_units
  USE fdf
  use units_m, only: inquire_unit
  USE fdf_prec
  implicit none
  
  real(dp)                   :: radius, mass

! Initialize
  call fdf_init('units-test.fdf', 'units-test.out')

  call fdf_set_unit_handler(inquire_unit)

  radius = fdf_physical('Galaxy-radius', 100.d0, 'ly')
  write(6,*) 'Galaxy radius:', radius, " light years"

  mass = fdf_physical('body-mass', 5.0d20, 'Kg')
  write(6,*) 'Body-mass: ', mass, " Kg"

!  Serialize and re-generate structure

  block
    character(len=1), allocatable    :: bufferFDF(:)

    call fdf_serialize_struct(bufferFDF)
    call fdf_shutdown()
    call fdf_recreate_struct(bufferFDF)
    call fdf_set_started(.true.)
    call fdf_set_unit_handler(inquire_unit)
    DEALLOCATE(bufferFDF)
  end block

  write(6,*) '/&/&/ With re-generated structure:'

  radius = fdf_physical('Galaxy-radius', 100.d0, 'parsec')
  write(6,*) 'Galaxy radius:', radius, " parsecs"

  call fdf_shutdown()

END PROGRAM TEST_UNITS
