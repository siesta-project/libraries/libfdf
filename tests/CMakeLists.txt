
add_executable(
  fdf-tester
  sample.F90
)
add_executable(
  test-units
  test-units.f90 units.f90
)
add_executable(
  test-ambiguous-units
  test-ambiguous-units.f90 units-ambiguous.f90
)

target_link_libraries(
  fdf-tester
  PRIVATE
  "${PROJECT_NAME}-lib"
)
target_link_libraries(
  test-units
  PRIVATE
  "${PROJECT_NAME}-lib"
)
target_link_libraries(
  test-ambiguous-units
  PRIVATE
  "${PROJECT_NAME}-lib"
)

file(MAKE_DIRECTORY "${PROJECT_BINARY_DIR}/install_tests")

file(COPY units-test.fdf ambiguous-units.fdf sample.fdf coords.fdf Coords.dat Otherfile XY.fdf
     DESTINATION "${PROJECT_BINARY_DIR}/install_tests")

add_test(  NAME    fdf-sample-test
           COMMAND $<TARGET_FILE:fdf-tester>
	   WORKING_DIRECTORY "${PROJECT_BINARY_DIR}/install_tests"
	)

add_test(  NAME    fdf-units-test
           COMMAND $<TARGET_FILE:test-units>
	   WORKING_DIRECTORY "${PROJECT_BINARY_DIR}/install_tests"
	)

add_test(  NAME    fdf-ambiguous-units-test
           COMMAND $<TARGET_FILE:test-ambiguous-units>
	   WORKING_DIRECTORY "${PROJECT_BINARY_DIR}/install_tests"
	)

set_property (TEST fdf-sample-test
              PROPERTY PASS_REGULAR_EXPRESSION "Polar | Enabled")

set_property (TEST fdf-units-test
              PROPERTY PASS_REGULAR_EXPRESSION "472491909")

set_property (TEST fdf-ambiguous-units-test
              PROPERTY PASS_REGULAR_EXPRESSION "Ambiguous")
